package RT::Extension::CustomField::Markdown::Util;
use strict;
use warnings;

use base 'Exporter';
our @EXPORT = qw/safe_run_child/;

sub safe_run_child (&) {
    my $our_pid = $$;

    # situation here is wierd, running external app
    # involves fork+exec. At some point after fork,
    # but before exec (or during) code can die in a
    # child. Local is no help here as die throws
    # error out of scope and locals are reset to old
    # values. Instead we set values, eval code, check pid
    # on failure and reset values only in our original
    # process
    my ($oldv_dbh, $oldv_rth);
    my $dbh = $RT::Handle ? $RT::Handle->dbh : undef;
    $oldv_dbh = $dbh->{'InactiveDestroy'} if $dbh;
    $dbh->{'InactiveDestroy'} = 1 if $dbh;
    $oldv_rth = $RT::Handle->{'DisconnectHandleOnDestroy'} if $RT::Handle;
    $RT::Handle->{'DisconnectHandleOnDestroy'} = 0 if $RT::Handle;

    my ($reader, $writer);
    pipe( $reader, $writer );

    my @res;
    my $want = wantarray;
    eval {
        my $code = shift;
        local @ENV{ 'LANG', 'LC_ALL' } = ( 'C.UTF-8', 'C.UTF-8' );
        unless ( defined $want ) {
            $code->();
        } elsif ( $want ) {
            @res = $code->();
        } else {
            @res = ( scalar $code->() );
        }
        exit 0 if $our_pid != $$;
        1;
    } or do {
        my $err = $@;
        $err =~ s/^Stack:.*$//ms;
        if ( $our_pid == $$ ) {
            $dbh->{'InactiveDestroy'} = $oldv_dbh if $dbh;
            $RT::Handle->{'DisconnectHandleOnDestroy'} = $oldv_rth if $RT::Handle;
            die "System Error: $err";
        } else {
            print $writer "System Error: $err";
            exit 1;
        }
    };

    close($writer);
    $reader->blocking(0);
    my ($response) = $reader->getline;
    warn $response if $response;

    $dbh->{'InactiveDestroy'} = $oldv_dbh if $dbh;
    $RT::Handle->{'DisconnectHandleOnDestroy'} = $oldv_rth if $RT::Handle;
    return $want? (@res) : $res[0];
}

1;
