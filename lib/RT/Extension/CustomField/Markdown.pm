package RT::Extension::CustomField::Markdown;
use strict;
use warnings;
use utf8;

use base 'Exporter';

use IPC::Run3 0.036 'run3';
use CSS::Inliner;
use RT::Interface::Web; # Gives us HTML::Mason::Commands
use RT::Extension::CustomField::Markdown::Util 'safe_run_child';

BEGIN {
    our $VERSION = '0.01';
    our @EXPORT = qw/RenderMarkdown InlineCSS ScrubMarkdownHTML/;
}
=head1 NAME

RT-Extension-CustomField-Markdown - Displays Markdown in text custom fields as rich text

=head1 DESCRIPTION

This extension displays text area custom fields as rich text if a
Markdown rendering application is associated with them. It adds the
ability to associate arbitrary rendering applications with text area
custom fields.

=head1 RT VERSION

Works with RT 4.4

=head1 INSTALLATION

=over

=item C<perl Makefile.PL>

=item C<make>

=item C<make install>

May need root permissions

=item Edit your F</opt/rt4/etc/RT_SiteConfig.pm>

If you are using RT 4.4 or greater, add this line:

    Plugin('RT::Extension::CustomField::Markdown');

=item Clear your mason cache

    rm -rf /opt/rt4/var/mason_data/obj

=item Restart your webserver

=back

Follow the steps in L</"CONFIGURATION"> to complete installation.

=head1 CONFIGURATION

This extension uses C<Text::Markdown> to render original Markdown as
rich text. But you can add support for any markup engine by defining
C<%CustomFieldMarkdownEngines> in C<RT_SiteConfig.pm>. This hashref
stores arbitrary markup rendering application configurations. Any
application will work as long as it can be configured to...

=over

=item *

Take input from STDIN

=item *

Print output to STDOUT

=item *

Generate (X)HTML output

=back

Configure each markup application to behave this way by setting its
C<Options> in C<%CustomFieldMakdownEngines>.

For example, to define Markdown and Kramdown engines:

 # In RT_SiteConfig.pm, add entries for each markup rendering engine
 # you want to use. For example:

 Set(%CustomFieldMarkdownEngines,
        Kramdown => '/usr/bin/kramdown --input kramdown --output html',
        Commonmark => '/usr/bin/commonmarker --extension=table',
 );

This extension creates a dropdown menu in text area custom field
configuration pages. It populates the menu with the original Markdown
option and keys from C<%CustomFieldMarkdownEngines>. To render a custom
field as rich text on display, select a markup engine from the
custom field menu.

=cut

RT->AddStyleSheets('cfmd-preview.css');
RT->AddJavaScript('cfmd-preview.js');

sub RenderMarkdown {
    my $content = shift;
    my $engine = shift;
    my ($out, $err) = (undef, undef);

    my %options = (
        binmode_stdin => ":utf8",
        binmode_stdout => ":utf8",
        binmode_stderr => ":utf8"
    );

    my $command = RT->Config->Get("CustomFieldMarkdownEngines")->{$engine};
    RT->Logger->error("Markdown engine is not configured: $engine") unless $command;
    return undef unless $command;

    my @command = split /\s+/, $command;
    RT->Logger->error("Markdown engine does not exist or is not executable: $command")
        unless -x $command[0];
    return undef unless -x $command[0];

    local $SIG{'CHLD'} = 'DEFAULT';
    safe_run_child { run3(
                         \@command,
                         \$content,
                         \$out,
                         \$err,
                         \%options
                         ) };

    RT->Logger->error("Markdown engine $engine could not render Markdown: $err") if length $err;

    return $out;
}

sub InlineCSS {
    my $css = shift;
    my $html = shift;

    return $html unless ($css || length $css);

    my $inliner = new CSS::Inliner;

    $inliner->read({
        html =>'<html><style type="text/css">'.$css.'</style>'.$html.'</html>',
        charset => 'utf8'
    });

    my $styled_html = $inliner->inlinify();

    foreach my $content_warning ( @{$inliner->content_warnings} ) {
        RT->Logger->warn("Inlining CSS generated warning: $content_warning");
    }

    return $styled_html;
}

my $SCRUBBER;
sub ScrubMarkdownHTML {
    my $Content = shift;

    my $allowed_tags = RT->Config->Get("CustomFieldMarkdownAllowedTags");
    my $allowed_attributes = RT->Config->Get("CustomFieldMarkdownAllowedAttributes");

    local @HTML::Mason::Commands::SCRUBBER_ALLOWED_TAGS = @{$allowed_tags} if $allowed_tags;
    local %HTML::Mason::Commands::SCRUBBER_ALLOWED_ATTRIBUTES = %{$allowed_attributes} if $allowed_attributes;
    $SCRUBBER = HTML::Mason::Commands::_NewScrubber() unless $SCRUBBER;

    $Content = '' if !defined($Content);
    return $SCRUBBER->scrub($Content);
}

package RT::CustomField;

sub MarkdownAttribute {
    my $self = shift;
    my $key = shift;

    my $cf_id = $self->id;

    return undef unless $self->Type eq 'Text';

    my $attr = $self->FirstAttribute( 'CustomFieldMarkdownSettings' );

    if ( $attr && $attr->Content && exists $attr->Content->{$key} ) {
        return $attr->SubValue($key);
    }

    return undef;
}

sub SetMarkdownAttributes {
    my $self = shift;
    my %args = (@_);

    my ($ok, $msg) = (0, undef);

    my $name = 'CustomFieldMarkdownSettings';

    my %content = ();
    $content{Engine} = $args{Engine} unless !exists $args{Engine};
    $content{CSS} = $args{CSS} unless !exists $args{CSS};

    my $attr = $self->FirstAttribute( $name );

    if ( $attr ) {
        ($ok, $msg) = $attr->SetSubValues( %content );
    } else {
        ($ok, $msg) = $self->AddAttribute(
            Name => $name,
            Description => 'RT::Extension::CustomField::Markdown settings',
            Content => \%content,
        );
    }

    return ($ok, $msg);
}

package RT::ObjectCustomFieldValue;
use RT::Extension::CustomField::Markdown qw/RenderMarkdown InlineCSS/;

sub RenderedMarkdownContent {
    my $self = shift;

    my $content = $self->Content;
    return undef unless $content;

    my $engine = $self->CustomFieldObj->MarkdownAttribute("Engine");
    return undef unless $engine;

    return RenderMarkdown($content, $engine);
}

sub RenderedMarkdownContentWithStyles {
    my $self = shift;

    my $css = $self->CustomFieldObj->MarkdownAttribute("CSS") || "";

    my $html = $self->RenderedMarkdownContent;

    return InlineCSS($css, $html);
}

=head1 AUTHOR

Countercontrol Technology LLC E<lt>brian@countercontrol.techE<gt>

=head1 BUGS

All bugs should be reported via the web at

    L<gitlab.com|https://gitlab.com/AccessNowHelpline/rt-extension-customfield-markup/issues>.

=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2019 by Access Now Inc

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007

=cut

1;
