jQuery(document).ready(function () {

    function validateJSend(json) {
        if (json.hasOwnProperty("status")) {
            if (json.status === "success" || json.status === "fail") {
                if (json.hasOwnProperty("data")) {
                    return json;
                }
            }
            else if (json.status === "error" && json.hasOwnProperty("message")) {
                return json;
            }
        }
        throw "JSend: Invalid JSend response";
    }

    function getJSendData(json) {
        var jsend = validateJSend(json);
        if (jsend.status === "fail") {

            throw "Rendering failure: "+JSON.stringify(jsend.data);
        }
        if (jsend.status === "error") {
            throw "Rendering error: "+jsend.message;
        }
        return jsend.data;
    }

    var initCFMD = function initializeCFMDElements(index, element) {

        var $cfmd = jQuery(element);
        var cfid = $cfmd.data("cfid");
        var $editRadio = $cfmd.children("input.cfmd-tab-edit");
        var $previewRadio = $cfmd.children("input.cfmd-tab-preview");
        var $error = $cfmd.children(".cfmd-error");
        var $editPanel = $cfmd.children("textarea");
        var $previewPanel = $cfmd.children(".cfmd-panel-preview");

        var initialWdith = $editPanel.width();
        var initialHeight = $editPanel.height();

        var editContent;

        function copySize($source, $dest) {
            var width = $source.width() || initialWidth;
            var height = $source.height() || initialHeight;

            $dest.width(width);
            $dest.height(height);
        }

        function editMode() {
            copySize($previewPanel, $editPanel);
            $previewPanel.empty();
        }

        function previewMode () {

            $error.css("display", "none");

            fetch("/CustomFieldMarkdown/Preview.json",
                  {
                      method: "POST",
                      mode: "same-origin",
                      cache: "no-cache",
                      credentials: "same-origin",
                      headers: {
                          "Accept": "application/json, text/javascript",
                          "Content-Type": "application/x-www-form-urlencoded"
                      },
                      body: "CustomFieldID="+cfid+"&Content="+encodeURIComponent(editContent)
                  })
                .then(response => response.json())
                .catch(err => error("Fetch error: "+err))
                .then(data => getJSendData(data))
                .catch(err => error(err))
                .then(data => jQuery.parseHTML(data))
                .catch(err => error("HTML parse error: "+err))
                .then(function(previewHTML) {
                    $previewPanel.empty();
                    $previewPanel.append(previewHTML);
                    copySize($editPanel, $previewPanel);
                });
        }

        function error(message) {
            $error.text(message);
            $error.css("display", "block");
            $error.siblings(".cfmd-tab-edit").prop("checked", true);
        }

        /*
         *  Event handlers
         */
        $editPanel.on("input", function(event) {
            editContent = event.target.value;
        });

        $previewRadio.on("change", function () {
            previewMode(this);
        });

        $editRadio.on("change", function() {
            editMode(this);
        });

        /*
         *  Initialize interface
         */
        $editPanel.trigger("input");
        // Initialize computed value of the CF edit textarea to itself.
        // Prevents it from being computed when the tab edit radio is unchecked,
        // temporarily setting this value to a very large width, which the
        // preview panel would copy the first time the preview radio was checked.
        copySize($editPanel, $editPanel);
        copySize($editPanel, $previewPanel);
    };

    jQuery(".cfmd").each( initCFMD );

});
