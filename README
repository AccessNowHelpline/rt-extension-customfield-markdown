NAME
    RT-Extension-CustomField-Markdown - Displays Markdown in text custom
    fields as rich text

DESCRIPTION
    This extension displays text area custom fields as rich text if a
    Markdown rendering application is associated with them. It adds the
    ability to associate arbitrary rendering applications with text area
    custom fields.

RT VERSION
    Works with RT 4.4

INSTALLATION
    perl Makefile.PL
    make
    make install
        May need root permissions

    Edit your /opt/rt4/etc/RT_SiteConfig.pm
        If you are using RT 4.4 or greater, add this line:

            Plugin('RT::Extension::CustomField::Markdown');

    Clear your mason cache
            rm -rf /opt/rt4/var/mason_data/obj

    Restart your webserver

    Follow the steps in "CONFIGURATION" to complete installation.

CONFIGURATION
    This extension uses Text::Markdown to render original Markdown as rich
    text. But you can add support for any markup engine by defining
    %CustomFieldMarkdownEngines in RT_SiteConfig.pm. This hashref stores
    arbitrary markup rendering application configurations. Any application
    will work as long as it can be configured to...

    *   Take input from STDIN

    *   Print output to STDOUT

    *   Generate (X)HTML output

    Configure each markup application to behave this way by setting its
    Options in %CustomFieldMakdownEngines.

    For example, to define Markdown and Kramdown engines:

     # In RT_SiteConfig.pm, add entries for each markup rendering engine
     # you want to use. For example:

     Set(%CustomFieldMarkdownEngines,
            Kramdown => '/usr/bin/kramdown --input kramdown --output html',
            Commonmark => '/usr/bin/commonmarker --extension=table',
     );

    This extension creates a dropdown menu in text area custom field
    configuration pages. It populates the menu with the original Markdown
    option and keys from %CustomFieldMarkdownEngines. To render a custom
    field as rich text on display, select a markup engine from the custom
    field menu.

AUTHOR
    Countercontrol Technology LLC <brian@countercontrol.tech>

BUGS
    All bugs should be reported via the web at

        L<gitlab.com|https://gitlab.com/AccessNowHelpline/rt-extension-customfield-markup/issues>.

LICENSE AND COPYRIGHT
    This software is Copyright (c) 2019 by Access Now Inc

    This is free software, licensed under:

      The GNU General Public License, Version 3, June 2007

