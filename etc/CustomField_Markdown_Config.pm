# configuration for RT::Extension::CustomField::Markdown

Set( %CustomFieldMarkdownEngines, (
         Markdown => '/usr/bin/markdown',
         Kramdown => '/usr/bin/kramdown --input kramdown --output html',
         Commonmark => '/usr/bin/commonmarker'

         # Render Markdown tables:
         # Commonmark => '/usr/bin/commonmarker --extension=table'
     )
);

#Set( @CustomFieldMarkdownAllowedTags,
#     qw(
#     A B U P BR I HR BR SMALL EM FONT SPAN STRONG SUB SUP S DEL STRIKE H1 H2 H3 H4 H5
#     H6 DIV UL OL LI DL DT DD PRE BLOCKQUOTE BDO
#     )
#
#     # Allow tables in rendered Markdown fields:
#     # qw(
#     # A B U P BR I HR BR SMALL EM FONT SPAN STRONG SUB SUP S DEL STRIKE H1 H2 H3 H4 H5
#     # H6 DIV UL OL LI DL DT DD PRE BLOCKQUOTE BDO
#     # TABLE THEAD TBODY TFOOT TR TD TH
#     # )
#);

#Set( %CustomFieldMarkdownAllowedAttributes, (
#    # Match http, https, ftp, mailto and relative urls
#    # XXX: we also scrub format strings with this module then allow simple config options
#    href   => qr{^(?:https?:|ftp:|mailto:|/|__Web(?:Path|HomePath|BaseURL|URL)__)}i,
#    face   => 1,
#    size   => 1,
#    color  => 1,
#    target => 1,
#    style  => qr{
#        ^(?:\s*
#            (?:(?:background-)?color: \s*
#                    (?:rgb\(\s* \d+, \s* \d+, \s* \d+ \s*\) |   # rgb(d,d,d)
#                       \#[a-f0-9]{3,6}                      |   # #fff or #ffffff
#                       [\w\-]+                                  # green, light-blue, etc.
#                       )                            |
#               text-align: \s* \w+                  |
#               font-size: \s* [\w.\-]+              |
#               font-family: \s* [\w\s"',.\-]+       |
#               font-weight: \s* [\w\-]+             |
#
#               border-style: \s* \w+                |
#               border-color: \s* [#\w]+             |
#               border-width: \s* [\s\w]+            |
#               padding: \s* [\s\w]+                 |
#               margin: \s* [\s\w]+                  |
#            )\s* ;? \s*)
#         +$ # one or more of these allowed properties from here 'till sunset
#    }ix,
#
#    # Add this clause to the 'style' regex to allow the
#    # text-decoration style for underlines and crossthroughs.
#    #          text-decoration: \s* [\w.\-\s]+      |
#
#    dir    => qr/^(rtl|ltr)$/i,
#    lang   => qr/^\w+(-\w+)?$/,
#    )
#);

# Set the extraAllowedContent config option for CKEditor instances on the ticket update page.
#
# See https://ckeditor.com/docs/ckeditor4/latest/guide/dev_allowed_content_rules.html for CKEditor
# allowed content rule format.
#
# This is a simple rule to allow only color styles on any tag:
#
# *{color}
#
# To make this even safer, you could specify only the tags we apply color styles to in
# your Markdown custom field CSS:
#
# h1 h2 a{color}
#
Set( $CustomFieldMarkdownCKEextraAllowedContent, '' );
